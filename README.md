# 2AM Takeout

## Tasks

### Setup
1. Read this whole README.
1. Fork this repo (you might need to make a Gitlab account first). Make sure your fork is _public_, not private.

### Coding
1. There is a test for the backend that is failing, fix the code so it passes.
1. There is a test for the backend where the assumptions about what needs to be tested is incorrect. The test passes, but doesn't guarantee compatibility with how the frontend expects to communicate. You need to think about how the two interact and are compatible, and change the test code to match.
1. In the frontend, make it so the user can enter a name that gets sent with every message and displayed to other users.

### Turning it in
1. When you are done, bundle your repo code into a gzip'd tar file (`$ tar --help`). Exclude the node_modules directory. You should have `two-am-takeout.tar.gz`.
  * Upload the tar file to your repo's package registry.
    * The docs for uploading a generic package are here: https://docs.gitlab.com/ee/user/packages/generic_packages/index.html
    * You can get an access token on your Gitlab profile.
    * You can get your Project ID on `Settings > General` on your project page.
1. Email tech@upchieve.org with the address of your fork in Gitlab.
1. Make sure you also push up your changes as commits in Gitlab so we can see your diffs vs the original.

### Bonus Points

1. There is a security issue in the socket server. Identify and fix it. (Hint, it's not a problem in the health check).
1. Make the frontend look nicer.

## Frontend

Install Node, we recommend using [nvm](https://github.com/nvm-sh/nvm) so you can set your version to v12.20.2.

### Project setup
```
$ npm install
```

#### Compiles and hot-reloads for development
```
$ npm run serve
```

## Server

Install [Go](https://golang.org/doc/install). This project was created using 1.15.8.

### Project setup
```
$ go mod download
```

#### Starting the server
```
$ go run ./server
```

#### Running the tests
```
$ go test ./server -v
```

## Resources/Notes

*Doing everything perfectly is not necessary to pass. Do your best. Timebox your effort to 2 hours. We respect your time.*

On the other hand, take notes in Notes.md! Add comments to the code! Show us your thought process. That way if you don't finish it in 2 hours, we know why and how you got where you did!

Our main goal is to assess how well you can step into an unfamiliar code base, read it, and understand it well enough to change it effectively.

We've picked Go for the backend because Go is optimized for readability and clarity. Even if you're not familiar with Go,
you should be able to make the small changes necessary to complete the project with a little research.

Similarly, we chose Vue both because we use it at UPchieve, but also because it offers a pretty straightforward html/js syntax with simple bindings. For a quick start, check out the [Vue Instance documentation](https://vuejs.org/v2/guide/instance.html) and follow directly on to the [Template Syntax documentation](https://vuejs.org/v2/guide/syntax.html).

The frontend uses [Vuetify](https://vuetifyjs.com/en/). You can find all the components there that you need to do the frontend work.

The backend uses the [Gorilla web toolkit](https://www.gorillatoolkit.org/), in particular the [mux](https://github.com/gorilla/mux) and [websocket](https://github.com/gorilla/websocket) packages.

We recommend installing the Go extension for VSCode or whatever IDE/editor you prefer to get intellisense for the Go files.
It might help you more quickly identify changes you can make.
