package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"gitlab.com/upchieve/two-am-takeout/server/handlers"
)

var ctx context.Context
var cancel context.CancelFunc

type ServerSuite struct {
	suite.Suite
	ctx    context.Context
	cancel context.CancelFunc
}

func (suite *ServerSuite) SetupSuite() {
	ctx = context.Background()
	suite.ctx, suite.cancel = context.WithCancel(ctx)

	go startServer(ctx)

	time.Sleep(1000)
}

func (suite *ServerSuite) TeardownSuite() {
	suite.cancel()
}

// one of these tests are testing something incorrect -
// what is this one even testing?
// it looks like this one is just checking to see if
// the server is running ****
func (suite *ServerSuite) TestHealthCheck() {
	resp, err := http.Get("http://localhost:3000/health")
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), http.StatusOK, resp.StatusCode)

	respData, err := ioutil.ReadAll(resp.Body)
	assert.Nil(suite.T(), err)

	var respBody handlers.HealthBody
	err = json.Unmarshal(respData, &respBody)
	assert.Nil(suite.T(), err)

	// make sure health body works matches what the frontend is expecting
	// looks like this functions similar to any test suite - 
	// Setup, Execution, Assertion, Teardown
	// this is my error - what does respString mean?
	// it is the response string 
	respString := string(respData)
	assert.Contains(suite.T(), respString, "{\"OK\":true}\n")
}


// why are we setting up a reader and a sender in this test - 
// it looks like when the server is running there should be some data available
// like a reader or a sender maybe
// but the server is giving me "Bad Request"
// now i see that it should probably say "Hello, Websocket!"
// where does "Bad Request" error come from?
// there may be an issue with the url - 
// i think the issue is that we're assuming that we want to test whether the 

// what i think i know now:
	// i want to test how the messages are flowing from frontend to backend
	// i need a way to verify if the reader and sender are even a thing
func (suite *ServerSuite) TestWebsockets() {
	u := url.URL{Scheme: "ws", Host: "localhost:3000", Path: "/ws"}

	reader, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer reader.Close()
	assert.Nil(suite.T(), err)

	sender, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer sender.Close()
	assert.Nil(suite.T(), err)

	// checking that text formatted messages go through and can be read correctly
	// how can we test this if we're getting a bad request
	// *** probably should start by making sure were getting a 200 level response at this point
	// how do we do that?
		//1. we know the server is running

	sender.WriteMessage(websocket.TextMessage, []byte("Hello, Websocket!"))

	_, message, err := reader.ReadMessage()
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), "Hello, Websocket!", string(message))
	// i added a failing test just to kind of get a better grasp of how the testing suite works
	assert.Contains(suite.T(), reader, "{\"OK\":true}\n")
}

func TestServerSuite(t *testing.T) {
	suite.Run(t, new(ServerSuite))
}
